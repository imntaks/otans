<?php

use Illuminate\Http\Request;
use App\Tax;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

// Route::middleware('api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('taxes', 'Api\TaxController@index');
Route::post('taxes', 'Api\TaxController@store');
Route::get('taxes/{tax}', 'Api\TaxController@edit');
Route::put('taxes/{tax}', 'Api\TaxController@update');
Route::post('taxes/{tax}/activate', 'Api\TaxController@activate');
Route::post('taxes/{tax}/deactivate', 'Api\TaxController@deactivate');

Route::get('products', 'Api\ProductController@index');
Route::post('products', 'Api\ProductController@store');
Route::get('products/{product}', 'Api\ProductController@edit');
Route::put('products/{product}', 'Api\ProductController@update');
Route::post('products/{product}/activate', 'Api\ProductController@activate');
Route::post('products/{product}/deactivate', 'Api\ProductController@deactivate');

Route::get('orders', 'Api\OrderController@index');
Route::post('orders', 'Api\OrderController@store');
Route::get('orders/{order}', 'Api\OrderController@edit');
Route::put('orders/{order}', 'Api\OrderController@update');

