<?php

use Illuminate\Database\Seeder;

class ProductTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tax = App\Tax::first();
        $products = factory(App\Product::class,10)->make();
        $products->each(function($p) use ($tax) {
            $p->applyTaxComponent($tax);
            $p->save();
        });
    }
}
