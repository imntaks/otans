<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
	protected $fillable = ['product_id', 'quantity'];

    public function Order()
    {
    	return $this->belongsTo(Order::class);
    }
    public function product()
    {
    	return $this->belongsTo(Product::class);
    }
    public function updateTaxTotal(Product $product) {
    	$this->tax_amount = round($this->quantity * $product->tax_amount,2);
    	return $this;
    }
    public function updateSubTotal(Product $product){
    	$this->sub_total = round($this->quantity * $product->list_price,2);
    	return $this;	
    }
}
