<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Address;
use App\Order;
use App\User;

class AddressController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return $request->user->addresses;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $address = new Address;
        $address->address_name = $request->get('address_name');
        $address->address_to = $request->get('address_to');
        $address->address_line_1 = $request->get('address_line_1');
        $address->address_line_2 = $request->get('address_line_2');
        $address->address_city = $request->get('address_city');
        $address->address_postcode = $request->get('address_postcode');
        $address->address_country = $request->get('address_country');
        $address->is_default = $request->get('is_default');

        $request->user->addresses()->save($address);
        return $address;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Address $address)
    {
        $existingAddress = Address::find($address->id);
        $existingAddress->address_name = $request->get('address_name');
        $existingAddress->address_to = $request->get('address_to');
        $existingAddress->address_line_1 = $request->get('address_line_1');
        $existingAddress->address_line_2 = $request->get('address_line_2');
        $existingAddress->address_city = $request->get('address_city');
        $existingAddress->address_postcode = $request->get('address_postcode');
        $existingAddress->address_country = $request->get('address_country');
        $existingAddress->is_default = $request->get('is_default');

        $existingAddress->save();
        return $existingAddress;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return "Not Implemented";
    }
}
